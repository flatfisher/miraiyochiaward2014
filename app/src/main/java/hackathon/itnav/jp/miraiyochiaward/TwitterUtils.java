package hackathon.itnav.jp.miraiyochiaward;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by kyo-tsuda on 2014/09/21.
 */
public class TwitterUtils {

    private static final String TOKEN = "token";
    private static final String TOKEN_SECRET = "token_secret";
    private static final String PREF_NAME = "twitter_access_token";


    public static Twitter getTwitterInstance(Context context) {
        String consumerKey = "g9EnY6RHNupBBfH42c6W7Cxnl";
        String consumerSecret = "jMB5cCngc8Q3w00KjFgPhsKZis4QPW94eQoLwSbISvy1Wyw4si";

        TwitterFactory factory = new TwitterFactory();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);

        if (hasAccessToken(context)) {
            twitter.setOAuthAccessToken(loadAccessToken(context));
        }
        return twitter;
    }


    public static void storeAccessToken(Context context, AccessToken accessToken) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(TOKEN, accessToken.getToken());
        editor.putString(TOKEN_SECRET, accessToken.getTokenSecret());
        editor.commit();
    }

    public static AccessToken loadAccessToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        String token = preferences.getString(TOKEN, null);
        String tokenSecret = preferences.getString(TOKEN_SECRET, null);
        if (token != null && tokenSecret != null) {
            return new AccessToken(token, tokenSecret);
        } else {
            return null;
        }
    }

    public static boolean hasAccessToken(Context context) {
        return loadAccessToken(context) != null;
    }

    public static void tweet(final Context context, final String text) {
       final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("ツイート中");
        progressDialog.setProgressStyle(AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        progressDialog.show();

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                Twitter twitter = TwitterUtils.getTwitterInstance(context);
                try {
                    twitter.updateStatus(text);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    if (e.isCausedByNetworkIssue()) {
                        Toast.makeText(context, "ネットーワークの問題です", Toast.LENGTH_LONG);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String url) {
                progressDialog.cancel();
            }
        };
        task.execute();


    }
}


/* How to use.
 if (!TwitterUtils.hasAccessToken(this)) {
           Intent intent = new Intent(this, TwitterOAuthActivity.class);
            startActivity(intent);
            finish();
        }

        TwitterUtils.tweet(this,"未来予知アワードテスト");
        Intent intent = new Intent(this,MainActivity.class);
        Resources resources = getResources();
        Bitmap bigPicture =  BitmapFactory.decodeResource(resources,R.drawable.ic_launcher);

 */