package hackathon.itnav.jp.miraiyochiaward;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by kyo-tsuda on 2014/09/21.
 */
public class FindNegative {

    private Context context;

    public FindNegative(Context context) {
        this.context = context;
    }


    public JSONObject find(String tweetText) {
        loadJsonArrayFromAssets();
        JSONObject jsonObject = findNagativeWordFromJsonArray(tweetText);
        if(jsonObject!=null){
            return jsonObject;
        }
        return null;

    }

    private JSONObject findNagativeWordFromJsonArray(String tweetText) {

        JSONArray jsonArray = loadJsonArrayFromAssets();
        int count = jsonArray.length();
        String nagativeword ;
        try {
            for (int i = 0; i < count; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                nagativeword  = jsonObject.getString("word");
                if(tweetText.indexOf(nagativeword ) != -1){
                   Log.v("sameWord", "Passed");
                    return jsonObject;
                }
            }
        } catch (JSONException e) {
            Log.v("",""+e);
        }
        return null;
    }


    private JSONArray loadJsonArrayFromAssets() {
        AssetManager assetManager = this.context.getResources().getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open("nagativeword.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer);
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("negative");
            return jsonArray;
        } catch (IOException e) {
            Log.v("IOException",""+e);
        }catch (JSONException e){
            Log.v("JSONException",""+e);
        }
        return null;
    }

}
