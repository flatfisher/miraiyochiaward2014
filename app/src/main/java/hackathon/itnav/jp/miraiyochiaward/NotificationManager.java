package hackathon.itnav.jp.miraiyochiaward;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * Created by kyo-tsuda on 2014/09/21.
 */
public class NotificationManager {

    private PendingIntent pendingIntent;
    private Context context;

    private int smallIcon;
    private String contentTitle;
    private String contentText;
    private Bitmap bigPicture;
    private String bigTitle;
    private String bigText;
    private int notificationId;

    private NotificationCompat.Builder notificationBuilder;

    public NotificationManager(Context context, Intent notificationIntent) {
        this.context = context;
        this.pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

    }

    public void setSmallIcon(int smallIcon) {
        this.smallIcon = smallIcon;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public void setBigPicture(Bitmap bigPicture) {
        this.bigPicture = bigPicture;
    }

    public void setBigTitle(String bigTitle) {
        this.bigTitle = bigTitle;
    }

    public void setBigText(String bigText) {
        this.bigText = bigText;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public void init() {
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this.getContext())
                        .setSmallIcon(this.getSmallIcon())
                        .setContentTitle(this.getContentTitle())
                        .setContentText(this.getContentText())
                        .setContentIntent(this.getPendingIntent())
                        .setDefaults(Notification.DEFAULT_SOUND
                        |Notification.DEFAULT_LIGHTS
                        |Notification.DEFAULT_VIBRATE)
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(this.getBigPicture())
                                .setBigContentTitle(getBigTitle())
                                .setSummaryText(this.getBigText()));

        this.notificationBuilder = notificationBuilder;
    }

    public void startNotify() {
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this.getContext());
        notificationManager.notify(this.getNotificationId(), this.getNotificationBuilder().build());
    }

    private Context getContext() {
        return this.context;
    }

    private PendingIntent getPendingIntent() {
        return this.pendingIntent;
    }

    private int getSmallIcon() {
        return this.smallIcon;
    }

    private String getContentTitle() {
        return this.contentTitle;
    }

    private String getContentText() {
        return this.contentText;
    }

    private Bitmap getBigPicture() {
        return this.bigPicture;
    }

    private String getBigTitle() {
        return this.bigTitle;
    }

    private String getBigText() {
        return this.bigText;
    }

    private int getNotificationId() {
        return this.notificationId;
    }

    private NotificationCompat.Builder getNotificationBuilder() {
        return this.notificationBuilder;
    }


    /* How to use.
        Intent intent = new Intent(this,MainActivity.class);
        Resources resources = getResources();
        Bitmap bigPicture =  BitmapFactory.decodeResource(resources,R.drawable.ic_launcher);

        NotificationManager notificationManager = new NotificationManager(this,intent);
        notificationManager.setSmallIcon(R.drawable.ic_launcher);
        notificationManager.setContentTitle("イトナブ");
        notificationManager.setContentText("石巻");
        notificationManager.setBigPicture(bigPicture);
        notificationManager.setBigTitle("未来予知アワード");
        notificationManager.setBigText("ストレスフリーク");
        notificationManager.setNotificationId(1107);

        notificationManager.init();
        notificationManager.startNotify();
     */
}
