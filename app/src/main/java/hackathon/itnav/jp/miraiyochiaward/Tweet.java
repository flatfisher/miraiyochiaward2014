package hackathon.itnav.jp.miraiyochiaward;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;


public class Tweet extends Activity implements TextWatcher {

    private EditText editText;
    private String tweetText = null;
    private FindNegative findNegative;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        findNegative = new FindNegative(this);

        Intent intent = new Intent(this,MainActivity.class);
        notificationManager = new NotificationManager(this,intent);

        editText = (EditText) findViewById(R.id.editText);
        editText.setText("");
        editText.addTextChangedListener(this);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tweet, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tweet:
                tweet();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    private void tweet() {
        if(this.tweetText!=null){
            TwitterUtils.tweet(this, this.tweetText);
            JSONObject nagativeWord =  this.findNegative.find(this.tweetText);
            if(nagativeWord!=null){
                startNotification(nagativeWord);
            }
        }
    }

    private void startNotification(JSONObject nagativeWord){
        String title = null;
        String message = null;
        String imagename = null;
        try {
            title = nagativeWord.getString("title");
            message = nagativeWord.getString("message");
            imagename = nagativeWord.getString("imagename");
        }catch (JSONException e){

        }

        Bitmap bigPicture = getResourceBitmap(imagename);

        notificationManager.setSmallIcon(R.drawable.icon);
        notificationManager.setContentTitle(title);
        notificationManager.setContentText(message);
        notificationManager.setBigPicture(bigPicture);
        notificationManager.setBigTitle(title);
        notificationManager.setBigText(message);
        notificationManager.setNotificationId(1107);

        notificationManager.init();
        notificationManager.startNotify();
    }

    private Bitmap getResourceBitmap(String imagename){
        int resourceid = getResources().getIdentifier(imagename,"drawable", getPackageName());
        Resources resources = getResources();
        Bitmap bigPicture =  BitmapFactory.decodeResource(resources, resourceid);
        return bigPicture;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
            this.tweetText = s.toString();
    }




}
